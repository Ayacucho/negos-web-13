import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { Route, RouterModule } from '@angular/router';

const exampleRoutes: Route[] = [
  {
      path     : 'Home',
      component: HomeComponent
  },

];

@NgModule({
  declarations: [
    HomeComponent
  ],
  imports: [
    RouterModule.forChild(exampleRoutes),
    CommonModule
  ]
})
export class InicioModule { }
