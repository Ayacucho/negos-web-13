import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagesComponent } from './pages/pages.component';
import { LoginComponent } from './shared/login/login.component';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'login', component: LoginComponent },
  {
    path: 'Latam',
    component: PagesComponent,
    children:
      [
        {
          path: 'Inicio',
          loadChildren:  () => import('./pages/inicio/inicio.module').then(m => m.InicioModule),
          // canActivate: [AuthGuard],
        },
      ]
    }
  ]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
