import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-footer',
//   templateUrl: './footer.component.html',
//   styleUrls: ['./footer.component.scss']
// })

@Component({
  selector: 'app-footer',
  template: `
      <div class="layout-footer clearfix">
          <span class="footer-text-right">
              <span class="fa fa-copyright"></span>
              <span>All Rights Reserved</span>
          </span>
      </div>
  `
})
export class FooterComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
