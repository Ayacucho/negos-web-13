import { ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { SocialUser,SocialAuthService } from 'angularx-social-login';
import { Observable, of } from 'rxjs';
import { Router } from '@angular/router';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import { LoginComponent } from './login.component';

let socialAuthtSpy: { authState: Observable<SocialUser>, initState:Observable<boolean> ; }
let router: Router;
let fixture: ComponentFixture<LoginComponent>;
let component: LoginComponent;


const socialUserMock:SocialUser = {
  authToken: "ya29.a0ARrdaM_ROM6MDviUaFjwxGqnsLgbnk9EDUoosksytMWZ1RhsjwPae9MaZ-k7O_BkBgS4n4JbxdW_OtMRuwMj2-zPwRWNNEijZGgHNj6wK2Zp5QktJ9c6cdWgTIBcBRG4ptsG99vPfCwvKyWYOUlPHlu7gcbgvQ",
  email: "gatoab2@gmail.com",
  firstName: "Felix",
  id: "109384422477251571759",
  idToken: "eyJhbGciOiJSUzI1NiIsImtpZCI6IjI3YzcyNjE5ZDA5MzVhMjkwYzQxYzNmMDEwMTY3MTM4Njg1ZjdlNTMiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiMTE5MTI4NTMzNzI3LTg3cWZjcDRhaGg2OWxnbWozbDR2bGl1ZzI4czhxaHJrLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiMTE5MTI4NTMzNzI3LTg3cWZjcDRhaGg2OWxnbWozbDR2bGl1ZzI4czhxaHJrLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTA5Mzg0NDIyNDc3MjUxNTcxNzU5IiwiZW1haWwiOiJnYXRvYWIyQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhdF9oYXNoIjoidHNvOGFrVWNFaG1pTTBfVENlSW5MZyIsIm5hbWUiOiJGZWxpeCBBU1RPIEJFUlJPQ0FMIiwicGljdHVyZSI6Imh0dHBzOi8vbGgzLmdvb2dsZXVzZXJjb250ZW50LmNvbS9hLS9BT2gxNEdqdERONXVxY2VLT3VaVnlLYW9GV0dENXAwQ29Nc0h5Q250LWV0cD1zOTYtYyIsImdpdmVuX25hbWUiOiJGZWxpeCIsImZhbWlseV9uYW1lIjoiQVNUTyBCRVJST0NBTCIsImxvY2FsZSI6ImVzIiwiaWF0IjoxNjM2NzIyNDA0LCJleHAiOjE2MzY3MjYwMDQsImp0aSI6IjNkMzM2MTUwY2E0YmYxYTQ2NzgxZjljMWZiZDBhNGNmYTRkMmNkNTgifQ.nRTPz1BqJahk-0lmWxHU8HG5IGnE8a5hVFn3JdqbCZ9bcTr3j9AMVZD3DzRHY6xycHzVCpOQlLndNEwfXwC3GGDPwMtdsR2590yPWb66jttCkEVfE-2jSmIo4SZ5Mluzc0Bam_x1A-SHHVSvD1ZTH85tcxvzoVl6K_80_mSeoy5FyM2U5lgKSWbYfH00xnUsDQOBV_8aSKdYHyncb4PhkgZ28kDECyHuB7rtz_MO3vX6wiO5EI5MzpUm9EByiT1ED0ZNd3ptlPdZ9VApzW-RO0lJs5IGwcDr23ycFMvMEh1qUUa53jYVBSs3V7U-6De7_m16re78PDr8nnGzPb5luA",
  lastName: "ASTO BERROCAL",
  name: "Felix ASTO BERROCAL",
  photoUrl: "https://lh3.googleusercontent.com/a-/AOh14GjtDN5uqceKOuZVyKaoFWGD5p0CoMsHyCnt-etp=s96-c",
  provider: "GOOGLE",
  authorizationCode: "",
  response: {
    access_token: "ya29.a0ARrdaM_ROM6MDviUaFjwxGqnsLgbnk9EDUoosksytMWZ1RhsjwPae9MaZ-k7O_BkBgS4n4JbxdW_OtMRuwMj2-zPwRWNNEijZGgHNj6wK2Zp5QktJ9c6cdWgTIBcBRG4ptsG99vPfCwvKyWYOUlPHlu7gcbgvQ",
    expires_at: 1636726003532,
    expires_in: 3599,
    first_issued_at: 1636722404532,
    id_token: "eyJhbGciOiJSUzI1NiIsImtpZCI6IjI3YzcyNjE5ZDA5MzVhMjkwYzQxYzNmMDEwMTY3MTM4Njg1ZjdlNTMiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiMTE5MTI4NTMzNzI3LTg3cWZjcDRhaGg2OWxnbWozbDR2bGl1ZzI4czhxaHJrLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiMTE5MTI4NTMzNzI3LTg3cWZjcDRhaGg2OWxnbWozbDR2bGl1ZzI4czhxaHJrLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTA5Mzg0NDIyNDc3MjUxNTcxNzU5IiwiZW1haWwiOiJnYXRvYWIyQGdtYWlsLmNvbSIsImVtYWlsX3ZlcmlmaWVkIjp0cnVlLCJhdF9oYXNoIjoidHNvOGFrVWNFaG1pTTBfVENlSW5MZyIsIm5hbWUiOiJGZWxpeCBBU1RPIEJFUlJPQ0FMIiwicGljdHVyZSI6Imh0dHBzOi8vbGgzLmdvb2dsZXVzZXJjb250ZW50LmNvbS9hLS9BT2gxNEdqdERONXVxY2VLT3VaVnlLYW9GV0dENXAwQ29Nc0h5Q250LWV0cD1zOTYtYyIsImdpdmVuX25hbWUiOiJGZWxpeCIsImZhbWlseV9uYW1lIjoiQVNUTyBCRVJST0NBTCIsImxvY2FsZSI6ImVzIiwiaWF0IjoxNjM2NzIyNDA0LCJleHAiOjE2MzY3MjYwMDQsImp0aSI6IjNkMzM2MTUwY2E0YmYxYTQ2NzgxZjljMWZiZDBhNGNmYTRkMmNkNTgifQ.nRTPz1BqJahk-0lmWxHU8HG5IGnE8a5hVFn3JdqbCZ9bcTr3j9AMVZD3DzRHY6xycHzVCpOQlLndNEwfXwC3GGDPwMtdsR2590yPWb66jttCkEVfE-2jSmIo4SZ5Mluzc0Bam_x1A-SHHVSvD1ZTH85tcxvzoVl6K_80_mSeoy5FyM2U5lgKSWbYfH00xnUsDQOBV_8aSKdYHyncb4PhkgZ28kDECyHuB7rtz_MO3vX6wiO5EI5MzpUm9EByiT1ED0ZNd3ptlPdZ9VApzW-RO0lJs5IGwcDr23ycFMvMEh1qUUa53jYVBSs3V7U-6De7_m16re78PDr8nnGzPb5luA",
    idpId: "google",
    login_hint: "AJDLj6JUa8yxXrhHdWRHIV0S13cAfbTO7nZAcod1nriE7jnURWmdI0Y8r8COg4uQMBRHAR0-X6qTzIfbbZrCCWEe6hDhsp-ylA",
    scope: "email profile https://www.googleapis.com/auth/userinfo.email openid https://www.googleapis.com/auth/userinfo.profile",
    session_state: {extraQueryParams: {authuser: "0"}},
    token_type: "Bearer"
  }
}
describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('Prueba de cambios en ngOnInit', () => {
    socialAuthtSpy.authState = of(socialUserMock)
    socialAuthtSpy.initState = of(true)
    component.loggedIn = true;
    component.ngOnInit();
    fixture.detectChanges();
    }); 
    it('Prueba de cambios en signInWithGoogle', () => {
      socialAuthtSpy.authState = of(socialUserMock)
      component.signInWithGoogle();
      fixture.detectChanges();
      });  

    it('Bienvenido a Dispo Online', () => {
      const fixture = TestBed.createComponent(LoginComponent);
      fixture.detectChanges();
      const compiled = fixture.nativeElement as HTMLElement;
      expect(compiled.querySelector('p.bienvenido')?.textContent).toContain('Bienvenido a Dispo Online');
    });
    it('Para ingresar debe hacerlo con su cuenta LATAM', () => {
      const fixture = TestBed.createComponent(LoginComponent);
      fixture.detectChanges();
      const compiled = fixture.nativeElement as HTMLElement;
      expect(compiled.querySelector('p.ingresar')?.textContent).toContain('Para ingresar debe hacerlo con su cuenta LATAM');
    });
    it('Boton de Entrar con Google', () => {
      const fixture = TestBed.createComponent(LoginComponent);
      fixture.detectChanges();
      const compiled = fixture.nativeElement as HTMLElement;
      expect(compiled.querySelector('a')?.textContent).toContain('Entrar con Google');
    });
});
