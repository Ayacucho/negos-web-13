import { Component, OnInit } from '@angular/core';
import { GoogleLoginProvider, SocialAuthService, SocialUser } from "angularx-social-login";
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user =  new SocialUser();
  loggedIn= false;
  socialUser: SocialUser;
  tokenService: any;
  constructor(
    private authService: SocialAuthService,
    private _router: Router,
    // private oauthService: OauthService,
    ) { 
      this.socialUser = new SocialUser();
    }

  ngOnInit() {
    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
    });
  }

  // signInWithGoogle(): void {
  //   this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  //               this._router.navigate(['/Latam/Inicio/Home']);

  // }
  signInWithGoogle(): void {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(
      data => {
        this.socialUser = data;
        console.log(data);
        
        // const tokenGoogle = new TokenDto(this.socialUser.idToken);
        // this.oauthService.google(tokenGoogle).subscribe(
        //   res => {
            // this.tokenService.setToken(res.value);
            this.loggedIn = true;
            localStorage.setItem('user', JSON.stringify(data));
            localStorage.setItem('user-latam', JSON.stringify(data));
            this._router.navigate(['/Latam/Inicio/Home']);
        //   },
        //   err => {
        //     console.log(err);
        //     this.tokenService.logOut();
        //   }
        // );
      }
    ).catch(
      err => {
        console.log(err);
      }
    );
  }
}
