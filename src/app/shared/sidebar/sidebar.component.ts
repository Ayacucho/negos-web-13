import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PagesComponent } from '../../pages/pages.component';
import { SocialAuthService } from "angularx-social-login";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  constructor(
    private _router: Router,
    public app: PagesComponent,
    private authService: SocialAuthService
    ) { }

  ngOnInit(): void {
  }
  // signOut(){
  // }
  signOut(): void {
    this.authService.signOut();
    localStorage.clear();
    this._router.navigate(['/']);
  }
}
