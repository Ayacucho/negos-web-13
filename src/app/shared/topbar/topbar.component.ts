import { Component, OnInit } from '@angular/core';
import { PagesComponent } from 'src/app/pages/pages.component';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {
  user: SocialUser;

  constructor(
    public app: PagesComponent
  ) { 
    this.user= JSON.parse(localStorage.getItem('user')|| '{}');
   
  }
  
  ngOnInit(): void {
  }

}
export declare class SocialUser {
  provider: string;
  id: string;
  email: string;
  name: string;
  photoUrl: string;
  firstName: string;
  lastName: string;
  authToken: string;
  idToken: string;
  authorizationCode: string;
  response: any;
}
